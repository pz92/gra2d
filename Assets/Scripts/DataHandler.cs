﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class DataHandler : MonoBehaviour
{

    private static DataHandler instance;
    public static DataHandler Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<DataHandler>();
            }
            return instance;
        }
    }

    /// <summary>
    /// Lista stanów map (true - odblokowana, false - zablokowana)
    /// </summary>
    public List<bool> listStateMap;
    /// <summary>
    /// Nazwa wybranej postaic
    /// </summary>
    public string selectedCharacter;      
    /// <summary>
    /// Indeks wybranej mapy
    /// </summary>
    public int currentMap = 0;                 
    /// <summary>
    /// Lista najlepszych wyników, na wybranej mapie
    /// </summary>
    public List<int> listBestScore;        
    /// <summary>
    /// Ilość przeciwników na mapie
    /// </summary>
    public int countEnemy = 0;              
    /// <summary>
    /// Ilość przedmiotów na mapie
    /// </summary>
    public int countItems = 0;              
    /// <summary>
    /// List najlepszych wyników ogółem
    /// </summary>
    public List<int> listHighestScores;     
    /// <summary>
    /// Całkowita ilość punktów zdobyta na kolejno przechodzonych mapach
    /// </summary>
    public int totalPoints = 0;            
    /// <summary>
    /// Aktualnie aktywny język
    /// </summary>
    public string currentLanguage = "PL";   
    /// <summary>
    /// Tablica zawierająca pliki map
    /// </summary>
    public TextAsset[] arrayFileMap;       
    /// <summary>
    /// Lista zawierająca wczytaną mapę
    /// </summary>
    public List<string> listMapTxt = new List<string>();  

    /// <summary>
    /// Metoda wywoływana przed uruchomieniem gry
    /// </summary>
    void Awake()
    {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
            Destroy(gameObject);
    }

    /// <summary>
    /// Metoda służąca do przekonwetrowania pliku tekstowego, w którym zapisana jest mapa, do listy typu string
    /// </summary>
    /// <param name="ta">Plik mapy, który chcemy przekonwertować</param>
    public void TextAssetToList(TextAsset ta)
    {
        listMapTxt.Clear();
        var arrayString = Regex.Split(ta.text, "\n");
        foreach (var line in arrayString)
        {
            listMapTxt.Add(line);
        }
    }
}
