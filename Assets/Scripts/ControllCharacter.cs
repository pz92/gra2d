﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllCharacter : MonoBehaviour {

    /// <summary>
    ///Metoda umożliwiająca poruszanie postacią przy pomocy przyciskow ekranowych
    /// </summary>
    /// <param name="direction">Kierunek poruszania postacią, w lewo (-1) lub w prawo (1)</param>
    public void MoveBtn(float direction)
    {
        Player.Instance.direction = direction;
        Player.Instance.move = true;
    }

    /// <summary>
    /// Metoda wylaczajaca poruszanie postaci, wykonuje sie gdy puscimy klawisz prawo, lewo
    /// </summary>
    public void StopMove()
    {
        Player.Instance.direction = 0;
        Player.Instance.move = false;
    }

    /// <summary>
    /// Metoda umozliwiajaca skakanie postaci
    /// </summary>
    public void JumpBtn()
    {
        Player.Instance.Jump = true;
        Player.Instance.MyAnimator.SetTrigger("jump");
    }

    /// <summary>
    /// Metoda umozliwiajaca walke mieczem
    /// </summary>
    public void SwordBtn()
    {
        Player.Instance.swordAttack = true;
        Player.Instance.MyAnimator.SetTrigger("attack");
    }

    /// <summary>
    /// Metoda umozliwa rzucanie strzalami
    /// </summary>
    public void ThrowBtn()
    {
        Player.Instance.arrowAttack = true;
        Player.Instance.MyAnimator.SetTrigger("throw");
    }
}
