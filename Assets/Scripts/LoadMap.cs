﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Text.RegularExpressions;
using System;

public class LoadMap : MonoBehaviour {

    private static LoadMap instance;

    /// <summary>
    /// Tablica prefabów
    /// </summary>
    public GameObject[] arrayObject;    //tablica prefabów
    /// <summary>
    /// Tablica teł
    /// </summary>
    public Texture[] arrayBackground;



    /// <summary>
    /// Obiekt do którego dodawane są sąsiadujące elementy platformy
    /// </summary>
    GameObject EmptyObject;

    /// <summary>
    /// Panel wyświetlający tło gry
    /// </summary>
    [SerializeField]
    private GameObject backgroundPanel;


    /// <summary>
    /// Długość mapy
    /// </summary>
    public int lenghtMap;


    /// <summary>
    /// Nazwa tła mapy
    /// </summary>
    private string backgroud;

    /// <summary>
    /// Obiekt przechowujący obiekty zawierające w nazwie słowo enemy
    /// </summary>
    [SerializeField]
    private GameObject parentEnemy;

    /// <summary>
    /// Obiekt przechowujący obiekty zawierające w nazwie słowo ground
    /// </summary>
    [SerializeField]
    private GameObject parentGround;

    /// <summary>
    /// Obiekt przechowujący obiekty zawierające w nazwie słowo tree
    /// </summary>
    [SerializeField]
    private GameObject parentTree;

    /// <summary>
    /// Obiekt przechowujący pozostałe obiekty
    /// </summary>
    [SerializeField]
    private GameObject parentOther;

    public static LoadMap Instance
    {
        get
        {
            if (instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<LoadMap>();
            }
            return instance;
        }
    }

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start()
    {
        DataHandler.Instance.countEnemy = 0;
        DataHandler.Instance.countItems = 0;
        //tablica prefabów
        arrayObject = Resources.LoadAll<GameObject>("PrefabsElementsOfMap/");

        //tablica backgroundów
        arrayBackground = Resources.LoadAll<Texture>("UI/BackgroundGame");

        AddElementsToScene(DataHandler.Instance.listMapTxt);
        backgroud = DataHandler.Instance.listMapTxt[0];
        SetBackground(backgroud);
    }

    /// <summary>
    /// Metoda sluzaca do dodania do sceny gameScene obiektow zgodnie z zawartoscia listy
    /// </summary>
    /// <param name="listTXT">Lista zawierająca nazwy obiektów, które mają zostać dodane do sceny</param>
    void AddElementsToScene(List<string> listTXT)
    {
        int it = 2;
        string tmpMapElement;
        lenghtMap = int.Parse(DataHandler.Instance.listMapTxt[1]);
        for (int i = 0; i < 15; i++)
        {
            for (int j = 0; j < lenghtMap; j++)
            {
                tmpMapElement = listTXT[it];

                for (int x = 0; x < arrayObject.Length; x++)
                {
                    if (Regex.IsMatch(tmpMapElement, @arrayObject[x].name))
                    {
                        if (Regex.IsMatch(tmpMapElement, @"ground") || Regex.IsMatch(tmpMapElement, @"water"))
                        {
                            if (Regex.IsMatch(tmpMapElement, @"begin"))
                            {
                                EmptyObject = new GameObject();
                                Instantiate(arrayObject[x], new Vector3(j * 1.28f, -(i * 1.28f), 0), Quaternion.identity).transform.SetParent(EmptyObject.transform);    //dodanie obiektu na scene
                            }

                            if (Regex.IsMatch(tmpMapElement, @"middle") || Regex.IsMatch(tmpMapElement, @"end"))
                                Instantiate(arrayObject[x], new Vector3(j * 1.28f, -(i * 1.28f), 0), Quaternion.identity).transform.SetParent(EmptyObject.transform);    //dodanie obiektu na scene

                            if (Regex.IsMatch(tmpMapElement, @"end"))
                                AddCollider(EmptyObject);

                            if (!Regex.IsMatch(tmpMapElement, @"begin") && !Regex.IsMatch(tmpMapElement, @"middle") && !Regex.IsMatch(tmpMapElement, @"end"))
                                Instantiate(arrayObject[x], new Vector3(j * 1.28f, -(i * 1.28f), 0), Quaternion.identity).transform.SetParent(parentGround.transform);    //dodanie obiektu na scene
                        }

                        else if (Regex.IsMatch(tmpMapElement, @"tree"))
                            Instantiate(arrayObject[x], new Vector3(j * 1.28f, -(i * 1.28f) + 0.82f, 0), Quaternion.identity).transform.SetParent(parentTree.transform);   //dodanie obiektu na scene

                        else if (Regex.IsMatch(tmpMapElement, @"enemy"))
                        {
                            Instantiate(arrayObject[x], new Vector3(j * 1.28f, -(i * 1.28f) + 0.2f, 0), Quaternion.identity).transform.SetParent(parentEnemy.transform);    //dodanie obiektu na scene                             
                            DataHandler.Instance.countEnemy++;
                        }
                        else
                        {
                            Instantiate(arrayObject[x], new Vector3(j * 1.28f, -(i * 1.28f) - 0.012f, 0), Quaternion.identity).transform.SetParent(parentOther.transform);    //dodanie obiektu na scene
                            if (Regex.IsMatch(tmpMapElement, @"item"))
                                DataHandler.Instance.countItems++;
                        }
                    }
                }
                it++;
            }
        }
    }

    /// <summary>
    /// Medota sluzaca do dodania colliderow do grupy elementow
    /// </summary>
    /// <param name="parentOfElements">Nazwa obiektu, do którego chcemy przypisać collider</param>
    void AddCollider(GameObject parentOfElements)
    {
        float center;
        float left = 0;
        float right = 0;
        int countElements = 0;

        //sprawdzam ile elementow wchodzi w sklad EmptyObject
        foreach (Transform g in parentOfElements.GetComponentInChildren<Transform>())
            countElements++;

        //wyciagam pozycje lewego i prawego elementu w osi X oraz obliczam gdzie jest srodek miedzy nimi
        left = parentOfElements.GetComponentInChildren<Transform>().GetChild(0).transform.position.x;
        right = parentOfElements.GetComponentInChildren<Transform>().GetChild(countElements - 1).transform.position.x;
        center = left + (right - left) / 2;

        //dodaje collider do obiektu EmptyObject
        //collider
        BoxCollider2D bx = parentOfElements.AddComponent<BoxCollider2D>();
        bx.offset = new Vector2(center, parentOfElements.GetComponentInChildren<Transform>().GetChild(0).transform.position.y);
        bx.size = new Vector2(countElements * 1.28f, 1);

        //dodaje collider jako trigger do obiektu EmptyObject
        //trigger
        BoxCollider2D bx2 = parentOfElements.AddComponent<BoxCollider2D>();
        bx2.offset = new Vector2(center, parentOfElements.GetComponentInChildren<Transform>().GetChild(0).transform.position.y - .5f);
        bx2.size = new Vector2(countElements * 1.4f, .6f);
        bx2.isTrigger = true;

        //dodaje skrypt ignorowania kolizji postaci z platforma gdy wskakuje od dolu
        CollisionTrigger ct = parentOfElements.AddComponent<CollisionTrigger>();
        ct.platformCollider = bx;
        ct.platformTrigger = bx;
    }

    /// <summary>
    /// Meteoda sluzaca do ustawienia tla gry
    /// </summary>
    /// <param name="backGround">Nazwa tła jakie chcemy ustawić</param>
    void SetBackground(string backGround)
    {
        for (int i = 0; i < arrayBackground.Length; i++)
            if (Regex.IsMatch(arrayBackground[i].name, @backGround))
                backgroundPanel.GetComponent<Renderer>().material.SetTexture("_EmissionMap", arrayBackground[i]);
    }
}
