﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class Knife : MonoBehaviour {

    /// <summary>
    /// Szybkość poruszania się lecącego noża
    /// </summary>
    [SerializeField]
    private float speed;

    /// <summary>
    /// Rigidbody noża
    /// </summary>
    private Rigidbody2D MyRigidbody;


    /// <summary>
    /// Kierunek lotu noża
    /// </summary>
    private Vector2 direction;

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>    
    void Start ()
    {
        MyRigidbody = GetComponent<Rigidbody2D>();
	}

    /// <summary>
    /// MEtoda wywoływana w stałych odstępach czasowych
    /// </summary>
    void FixedUpdate()
    {
        MyRigidbody.velocity = direction * speed;
    }
	
    
    /// <summary>
    /// Metoda usuwająca obiekt, gdy wyleci poza pole widzenia kamery
    /// </summary>
    void OnBecameInvisible()
    {
        Destroy(gameObject);
    }

    /// <summary>
    /// Metoda określająca kierunek lotu noza
    /// </summary>
    /// <param name="direction">Wektor kierunku lotu noża</param>
    public void Initialize(Vector2 direction)
    {
        this.direction = direction;
    }
}
