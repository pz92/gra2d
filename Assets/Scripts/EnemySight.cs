﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySight : MonoBehaviour {

    //przypisać wzrok do enemy, bo odnosimy sie do jego metod w klasie Enemy
    [SerializeField]
    private Enemy enemy;

    //jesli player wejdzie w trigger (pole widzenia enemy)
	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            enemy.Target = other.gameObject;
        }
    }
    //jesli player wyjdzie z triggera (pole widzenia enemy)
    void OnTriggerExit2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            enemy.Target = null;
        }
    }
}
