﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Menu : MonoBehaviour {

    /// <summary>
    /// Lista paneli menu
    /// </summary>
    [SerializeField]
    private List<GameObject> listMenuPanels;
    /// <summary>
    /// Przycisk akceptacji postaci (potrzeby do walidacji)
    /// </summary>
    [SerializeField]
    private Button buttonAcceptCharacter;
    /// <summary>
    /// Label najlepszego wyniku danej mapy
    /// </summary>
    [SerializeField]
    private Text labelBestScore;
    /// <summary>
    /// Lista przycisków postaci
    /// </summary>
    [SerializeField]
    private List<GameObject> buttonCharacter;
    /// <summary>
    /// Panel wyświatlający zdjęcie mapy
    /// </summary>
    [SerializeField]
    private GameObject imagePanel;
    /// <summary>
    /// Przycisk efektów dźwiękowych
    /// </summary>
    [SerializeField]
    private Button sound;
    /// <summary>
    /// Panel ustawień
    /// </summary>
    [SerializeField]
    private GameObject settingsPanel;
    /// <summary>
    /// Przycisk wyboru języka
    /// </summary>
    [SerializeField]
    private GameObject langButton;
    /// <summary>
    /// Lista flag języków
    /// </summary>
    [SerializeField]
    private List<Sprite> langSprite;

    /// <summary>
    /// Aktualnie wybrana mapa
    /// </summary>
    private int  currentMap;
    private MapLoader mapLoader;

    /// <summary>
    /// Pole tekstowe wyświetlające informacje o wybranym osiągnięciu
    /// </summary>
    [SerializeField]
    private Text textInfoAchieve;
    /// <summary>
    /// Lista przycisków osiągnięć
    /// </summary>
    [SerializeField]
    private List<Button> listButtonsAchive = new List<Button>();


    public AudioClip clickButton;
    public AudioSource soundEffect;

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary> 
    void Start()
    {
        mapLoader = GameObject.Find("MapLoader").GetComponent<MapLoader>();
        currentMap = 0;
        changeMap();
        bestScores.Instance.UpdateListHighScores();
        AchievementsButtonsColor(new Color32(173, 173, 173, 255), new Color32(255, 255, 255, 255));
        buttonAcceptCharacter.interactable = false;
        AudioListener.pause = false;
    }

    /// <summary>
    /// Metoda umożliwiająca przełączanie się pomiędzy panelami menu
    /// </summary>
    /// <param name="id_Panel">Id panela, który chcemy aktywować</param>
    public void MenuPanelsManager(int id_Panel)
    {
        soundEffect.PlayOneShot(clickButton);
        foreach (GameObject panel in listMenuPanels)
            panel.SetActive(false);

        listMenuPanels[id_Panel].SetActive(true);

        if(id_Panel == 0)
            textInfoAchieve.text = null;

        settingsPanel.SetActive(false);
    }

    /// <summary>
    /// Metoda służaca do ustawienia koloru przycisków osiągnięć
    /// </summary>
    /// <param name="lockedAchievement">Kolor przycisku zablokowanego osiągnięcia</param>
    /// <param name="unlockedAchievement">Kolor przycisku odblokowanego osiągnięcia</param>
    void AchievementsButtonsColor(Color32 lockedAchievement, Color32 unlockedAchievement)
    {
        for (int i = 0; i < listButtonsAchive.Count; i++)
        {
            if (Achievements.Instance.ListAchievementState[i])
            {
                listButtonsAchive[i].GetComponent<Image>().color = unlockedAchievement;
            }
            else
            {
                listButtonsAchive[i].GetComponent<Image>().color = lockedAchievement;
            }
        } 
    }

    /// <summary>
    /// Metoda służaca do zmiany języka w grze
    /// </summary>
    public void LangBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        if (DataHandler.Instance.currentLanguage == "PL")
        {
            Language.Instance.SetLanguage("EN");
            DataHandler.Instance.currentLanguage = "EN";
            langButton.GetComponent<Image>().sprite = langSprite[0];
        }
        else
        {
            Language.Instance.SetLanguage("PL");
            DataHandler.Instance.currentLanguage = "PL";
            langButton.GetComponent<Image>().sprite = langSprite[1];
        }

    }

    /// <summary>
    /// Metoda służaca do włączenia lub wyłączenia muzyki w grze
    /// </summary>
    public void SoundBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        if (AudioListener.pause == false)
        {
            AudioListener.pause = true;
            sound.GetComponent<Image>().color = new Color(255, 255, 255, 0.5f);
        }
        else
        {
            AudioListener.pause = false;
            sound.GetComponent<Image>().color = new Color(255, 255, 255, 1);
        }
    }

    /// <summary>
    /// Metoda służąca do przejścia do sceny loadingScene
    /// </summary>
    public void PlayBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        if (DataHandler.Instance.listStateMap[currentMap])
        {
            SceneManager.LoadScene("loadingScene");
            DataHandler.Instance.TextAssetToList(DataHandler.Instance.arrayFileMap[currentMap]);
        }  
    }

    /// <summary>
    /// Metoda do zmiany mapy na następną
    /// </summary>
    public void NextMap()
    {
        soundEffect.PlayOneShot(clickButton);
        if (currentMap < DataHandler.Instance.arrayFileMap.Length - 1)
            currentMap++;
        else
            currentMap = 0;

        changeMap();

    }

    //jest wywoływana przyciskiem w lewo w oknie wyboru mapy
    /// <summary>
    /// Metoda do zmiany mapy na poprzenią,
    /// </summary>
    public void PrevMap()
    {
        soundEffect.PlayOneShot(clickButton);
        if (currentMap > 0)
            currentMap--;
        else
            currentMap = DataHandler.Instance.arrayFileMap.Length - 1;

        changeMap();    
    }

    /// <summary>
    /// Metoda służąca do zmieniania zdjęcia mapy, najlepszego wyniku
    /// </summary>
    private void changeMap()
    {
        if (DataHandler.Instance.listStateMap[currentMap])
        {
            imagePanel.GetComponent<Image>().color = new Color32(255, 255, 255, 255);
        }
        else
        {
            imagePanel.GetComponent<Image>().color = new Color32(255, 255, 255, 128);
        }

        labelBestScore.text =  DataHandler.Instance.listBestScore[currentMap].ToString();
        DataHandler.Instance.currentMap = currentMap;
        imagePanel.GetComponent<Image>().sprite = mapLoader.arrayPicturesMaps[currentMap];
    }

    /// <summary>
    /// Metoda służąca do zmiany koloru przycisku wybranej postaci, jednocześnie służąca jako walidacja wyboru postaci
    /// </summary>
    /// <param name="character">Nazwa wybranej postaci ("PlayerGirl" lub "PlayerBoy")</param>
    public void chooseCharacter(string character)
    {
        soundEffect.PlayOneShot(clickButton);
        if (character == "PlayerBoy")
        {
            DataHandler.Instance.selectedCharacter = "PlayerBoy";
            buttonCharacter[0].GetComponent<Image>().color = new Color(255, 255, 255, 255);
            buttonCharacter[1].GetComponent<Image>().color = new Color(255, 255, 255, 0.5f);
        }
        else
        {
            DataHandler.Instance.selectedCharacter = "PlayerGirl";
            buttonCharacter[0].GetComponent<Image>().color = new Color(255, 255, 255, 0.5f);
            buttonCharacter[1].GetComponent<Image>().color = new Color(255, 255, 255, 255);
        }
        buttonAcceptCharacter.interactable = true;

    }

    /// <summary>
    /// Metoda pokazująca i ukrywająca panel z przyciskami języka i muzyki w grze
    /// </summary>
    public void settingBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        if (settingsPanel.activeSelf)
        {
            settingsPanel.SetActive(false);
        }
        else
        {
            settingsPanel.SetActive(true);
        }
    }

    /// <summary>
    /// Metoda wyświetlająca informację co trzeba wykonać aby odblokować dane osiągnięcie
    /// </summary>
    /// <param name="id_button">Id przycisku, który został naciśnięty</param>
    public void showInfoAchievements(int id_button)
    {
        soundEffect.PlayOneShot(clickButton);
        textInfoAchieve.text = Achievements.Instance.ListAchievements[id_button];
    }
}
