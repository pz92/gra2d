﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character {

    /// <summary>
    /// Obecny stan postaci enemy
    /// </summary>
    private IEnemyState currentState;
    /// <summary>
    /// Cel postaci enemy
    /// </summary>
    [SerializeField]
    public GameObject Target { get; set; }

    /// <summary>
    /// Odległość enemy od playera kiedy ma atakować mieczem
    /// </summary>
    [SerializeField]
    private float meleeRange;

    /// <summary>
    /// Odległość enemy od playera kiedy ma atakować nożami
    /// </summary>
    [SerializeField]
    private float throwRange;

    /// <summary>
    /// Lista krawędzi w których enemy zmienia swój kierunek poruszania
    /// </summary>
    [SerializeField]
    public List<Transform> EdgeList;

    /// <summary>
    /// Po pokananiu przeciwnika wypadnie z niego przedmiot
    /// </summary>
    private bool giveItem = true;

    /// <summary>
    /// Lewa krawędź do której może dość enemy
    /// </summary>
    [SerializeField]
    private Transform EdgeLeft;

    /// <summary>
    /// Prawa krawędź do której może dość enemy
    /// </summary>
    [SerializeField]
    private Transform EdgeRight;

    /// <summary>
    /// Sprawdzenie czy gracz znajduje sie w dystansie do atakowania mieczem
    /// </summary>
    public bool InMeleeRange
    {
        get
        {
            if (Target != null)
            {
                //odległość enemy od playera
                return Vector2.Distance(transform.position, Target.transform.position) <= meleeRange;
            }

            return false;
        }
    }

    /// <summary>
    /// Sprawdzenie czy gracz znajduje sie w dystansie do atakowania nozem
    /// </summary>
    public bool InThrowRange
    {
        get
        {
            if (Target != null)
            {
                //odległość enemy od playera
                return Vector2.Distance(transform.position, Target.transform.position) <= throwRange;
            }

            return false;
        }
    }

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    public override void Start ()
    {
        base.Start();
        Player.Instance.Dead += new DeadEventHandler(RemoveTarget);
        ChangeState(new IdleState());
        FindEdges();
    }

    /// <summary>
    /// Motoda wykonuje się w określonych odstępach czasowych
    /// </summary>
    void FixedUpdate ()
    {
        if(!IsDead)
        {
            if(!TakingDamage)
            {
                currentState.Execute();
            }
            LookAtTarget();
        }
    }

    /// <summary>
    /// Metoda usuwa cel i powraca do stanu patrolu
    /// </summary>
    public void RemoveTarget()
    {
        Target = null;
        ChangeState(new PatrolState());
    }

    /// <summary>
    /// Metoda odwraca enemy w strone gracza
    /// </summary>
    private void LookAtTarget()
    {
        if (Target != null)
        {
            float xDir = Target.transform.position.x - transform.position.x;
            if (xDir < 0 && facingRight || xDir > 0 && !facingRight)
            {
                ChangeDirection();
            }
        }
    }

    /// <summary>
    /// Metoda odpowiadajaca za zmiane stanow przeciwnika
    /// </summary>
    /// <param name="newState"></param>
    public void ChangeState(IEnemyState newState)
    {
        if(currentState != null)
        {
            currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this);
    }

    /// <summary>
    /// Metoda odpowiadajaca za poruszanie sie przeciwnika
    /// </summary>
    public void Move()
    {
        if (!Attack)
        {
            //jesli enemy jest zwrocony w lewo i jego pozycja jest wieksza na X niz pozycja leftEdge to enemy moze sie poruszac, analogicznie z edgeRight
            if((!facingRight && transform.position.x > EdgeLeft.position.x) || (facingRight && transform.position.x < EdgeRight.position.x))
            {
                //ustawienie parametru spped na 1, animacja chodzenia
                MyAnimator.SetFloat("speed", 1);

                //obrócenie postaci w odpowiednik kierunku
                transform.Translate(GetDirection() * (movementSpeed * Time.deltaTime));
                //delta time żeby postać poruszała się z taką samą predkoscia na różnych komputerach 30fps / 60fps itd
            }
            //jesli jest w stanie partolowania to zmienia kirunek chodzenia
            else if (currentState is PatrolState)
            {
                ChangeDirection();
            }
            //jesli atakuje nozem i znikniemy z pola widzenia to zeruje target i zmianiam stan na idle
            else if(currentState is RangedState)
            {
                Target = null;
                ChangeState(new IdleState());
            }
        }
    }

    /// <summary>
    /// Metoda zwracająca wektor kierunku poruszania się enemy
    /// </summary>
    /// <returns></returns>
    public Vector2 GetDirection()
    {
        //if facingRight is true return  Vector2.right else return Vector2.left
        return facingRight ? Vector2.right : Vector2.left;
    }


    /// <summary>
    /// Metoda sprawdzajaca czy collider postaci zostal naruszony przez inny collider (other).
    /// </summary>
    /// <param name="other"></param>
    public override void OnTriggerEnter2D(Collider2D other)
    {
        //wywoluje metode z klasy nadrzednej
        base.OnTriggerEnter2D(other);

            //wywołuje metode onTriggerEnter w klasie obecnego stanu PatrolState or RangedState itd...

            currentState.OnTriggerEnter(other);

    }


    /// <summary>
    /// Metoda wywolywana, gdy player odniesie obrazania dokonane przez przeciwnika
    /// </summary>
    public override void TakeDamage()
    {
        health -= 10;

        if (!IsDead)
        {
            MyAnimator.SetTrigger("damage");
        }
        else
        {
            if(giveItem)
            {
                int itemIndex = UnityEngine.Random.Range(0, 4);
                Instantiate(GameManager.Instance.ItemsPrefab[itemIndex], new Vector2(transform.position.x, transform.position.y), Quaternion.identity);
                giveItem = false;
            }
            MyAnimator.SetTrigger("die");
        }
    }

    /// <summary>
    /// Metoda usuwająca obiekt jeśli enemy został pokonany
    /// </summary>
    public override void Death()
    {
        GameManager.Instance.Points += 500;
        DataHandler.Instance.totalPoints += 500;
        Destroy(gameObject);
        Achievements.Instance.UnlockAchieve(2);
        Player.Instance.killedEnemy++;
    }

    //sprawdzam czy umarł
    /// <summary>
    /// Metoda sprawdzająca czy enemy nie żyje
    /// </summary>
    public override bool IsDead
    {
        get
        {
            return health <= 0;
        }
    }

    /// <summary>
    /// Metoda znajdująca punktt, w których enemy zmienia kierunek poruszania się
    /// </summary>
    public void FindEdges()
    {
        //wrzicam wszystkie obiekty z tagiem Edge do listy
        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Edge"))
        {
             EdgeList.Add(obj.transform);
        }

        float minDistLeft = Mathf.Infinity;
        float minDistRight = Mathf.Infinity;
        Vector2 currentPos = transform.position;

        foreach(Transform edge in EdgeList)
        {
            float dist = Vector2.Distance(edge.position, currentPos);
            if(dist < minDistLeft && edge.transform.position.x < currentPos.x && (edge.transform.position.y <= currentPos.y && edge.transform.position.y >= currentPos.y - 1.5))
            {
                EdgeLeft = edge;
                minDistLeft = dist;
            }
            else if (dist < minDistRight && edge.transform.position.x > currentPos.x && (edge.transform.position.y <= currentPos.y && edge.transform.position.y >= currentPos.y - 1.5))
            {
                EdgeRight = edge;
                minDistRight = dist;
            }
        }

        EdgeList.Clear();
    }

}
