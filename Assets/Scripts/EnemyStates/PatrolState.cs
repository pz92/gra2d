﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : IEnemyState {

    private Enemy enemy;

    //do zliczania czasu patrolowania
    private float patrolTimer;

    //czas patrolowania
    private float patrolDuration;

    public void Enter(Enemy enemy)
    {
        patrolDuration = UnityEngine.Random.Range(2, 8);
        this.enemy = enemy;
    }

    public void Execute()
    {
        Patrol();
        enemy.Move();

        //jesli player jest w polu widzenia && w odpowiedniej odleglosci to enamy atakuje nozem 
        if(enemy.Target != null && enemy.InThrowRange)
        {
            enemy.ChangeState(new RangedState());
        }
    }

    public void Exit()
    {
    }

    public void OnTriggerEnter(Collider2D other)
    {

        if (other.tag == "PlayerKnife")
        {
            enemy.Target = Player.Instance.gameObject;
        }

    }

    private void Patrol()
    {
        patrolTimer += Time.deltaTime;

        if (patrolTimer >= patrolDuration)
        {
            enemy.ChangeState(new IdleState());
        }
    }
}
