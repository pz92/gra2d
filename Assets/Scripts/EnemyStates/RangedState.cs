﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedState : IEnemyState {

    private Enemy enemy;

    private float throwTimer;
    private float throwCoolDown;
    private bool canThrow = true;

    public void Enter(Enemy enemy)
    {
        throwCoolDown = UnityEngine.Random.Range(2, 4);
        this.enemy = enemy;
    }

    public void Execute()
    {
        ThrowKnife();

        //jesli rzuca nozami i podejdziemy blisko to zacznie atakowac mieczem
        if(enemy.InMeleeRange)
        {
            enemy.ChangeState(new MeleeState());
        }

        //jesli wykrywa playera to idzie w jego kierunku
        if(enemy.Target != null)
        {
            enemy.Move();
        }
        //jesli nie to stoi w miejscu
        else
        {
            enemy.ChangeState(new IdleState());
        }
    }

    public void Exit()
    {
    }

    public void OnTriggerEnter(Collider2D other)
    {
    }

    private void ThrowKnife()
    {
        throwTimer += Time.deltaTime;

        if(throwTimer >= throwCoolDown)
        {
            canThrow = true;
            throwTimer = 0;
        }

        if(canThrow)
        {
            canThrow = false;
            enemy.MyAnimator.SetTrigger("throw");
        }
    }

}
