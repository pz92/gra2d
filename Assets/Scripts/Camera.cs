﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour {

    private static Camera instance;

    /// <summary>
    /// Maksymalna wartość na osi X do której kamera może się przesunąć
    /// </summary>
    [SerializeField]
    private float xMax;
    /// <summary>
    /// Maksymalna wartość na osi Y do której kamera może się przesunąć
    /// </summary>
    [SerializeField]
    private float yMax;
    /// <summary>
    /// Minimalna wartość na osi X do której kamera może się przesunąć
    /// </summary>
    [SerializeField]
    private float xMin;
    /// <summary>
    /// Minimalna wartość na osi Y do której kamera może się przesunąć
    /// </summary>
    [SerializeField]
    private float yMin;

    /// <summary>
    /// Pozycja obiektu, który ma być śledzony przez kamerę
    /// </summary>
    private Transform target;

    /*
    public bool cameraMoving = false;
    private float tmp1;
    private float tmp2;
    */

    public static Camera Instance
    {
        get
        {
            if (instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<Camera>();
            }
            return instance;
        }

    }

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start () {
        //znalezienie obiektu z tagiem "player"
        target = GameObject.Find(DataHandler.Instance.selectedCharacter).transform;
        if (LoadMap.Instance.lenghtMap == 100)
            xMax = 118.33f;
        else if (LoadMap.Instance.lenghtMap == 200)
            xMax = 246.38f;
        else
            xMax = 374.31f;
    }

    /// <summary>
    /// Metoda uruchamiana jako ostatnia z metod typu Update()
    /// </summary>
    void LateUpdate () {

        //tmp1 = transform.position.x;
        //przypisanie kamerze pozycji, Mathf.Clamp umożliwia zmianę pozycji kamery w określonym zakresie xMin, xMax & yMin, yMax
        transform.position = new Vector3(Mathf.Clamp(target.position.x, xMin, xMax), Mathf.Clamp(target.position.y, yMin, yMax), transform.position.z);

        //potrzebne do scroll tła
        /*
        tmp2 = transform.position.x;
        if (tmp1 != tmp2)
            cameraMoving = true;
        else
            cameraMoving = false;
        */
    }
}
