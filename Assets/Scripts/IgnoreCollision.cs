﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollision : MonoBehaviour {

    /// <summary>
    /// Collider postaci gracza
    /// </summary>
    private Collider2D player;
    /// <summary>
    /// Lista skrzynek, które postać enemy ma unikać
    /// </summary>
    [SerializeField]
    private GameObject[] boxes;

    void Start()
    {
        boxes = GameObject.FindGameObjectsWithTag("box");
        foreach(GameObject g in boxes)
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), g.GetComponent<Collider2D>(), true);

    }
    /// <summary>
    /// Metoda wywoływana w stałych odstępach czasowych
    /// </summary>
    private void FixedUpdate ()
    {
        if(player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Collider2D>();
            Physics2D.IgnoreCollision(GetComponent<Collider2D>(), player, true);
        }
    }
}
