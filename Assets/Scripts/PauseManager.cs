﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseManager : MonoBehaviour {

    private static PauseManager instance;

    [SerializeField]
    private GameObject panelPause;
    [SerializeField]
    private GameObject panelFinish;
    [SerializeField]
    private Button sound;
    [SerializeField]
    public GameObject ButtonPlay;
    [SerializeField]
    public GameObject ControlButtonsPanel;

    public AudioClip clickButton;
    public AudioSource soundEffect;

    public static PauseManager Instance
    {
        get
        {
            if (instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<PauseManager>();
            }
            return instance;
        }
    }

    public GameObject PanelFinish
    {
        get
        {
            return panelFinish;
        }
    }


    //Metoda umozliwiajaca zapauzowanie gry
    //Wywolywana po nacisnieciu przyciusku pauzy
    public void PauseBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        Time.timeScale = 0.0F;
        panelPause.SetActive(true);
        ControlButtonsPanel.SetActive(false);
    }

    //Metoda umozliwiajaca ponowne zaladowanie aktualnego poziomu
    //Wywolywana po nacisnieciu przyciusku restart w menu pauzy lub menu finish
    public void RestartBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        DataHandler.Instance.totalPoints = 0;
        SceneManager.LoadScene("loadingScene");
        Time.timeScale = 1.0F;
    }

    //Metoda umozliwiajaca powrocenie do gry z menu pauzy
    //Wywolywana po nacisnieciu przyciusku play w menu pauzy
    public void PlayBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        Time.timeScale = 1.0F;
        ControlButtonsPanel.SetActive(true);
        panelPause.SetActive(false);
        PanelFinish.SetActive(false);
    }

    //Metoda umozliwiajaca wyciszenie dzwiekow gry
    //Wywolywana po nacisnieciu przyciusku sound w menu pauzy
    public void SoundBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        if (AudioListener.pause == false)
        {
            sound.GetComponent<Image>().color = new Color(255, 255, 255, 1);
            AudioListener.pause = true;
        }
        else
        {
            sound.GetComponent<Image>().color = new Color(255, 255, 255, 0.5f);
            AudioListener.pause = false;
        }
    }

    //Metoda umozliwiajaca powrot do menu glownego gry
    //Wywolywana po nacisnieciu przyciusku backToMenu w menu pauzy lub menu finish
    public void BackToMenuBtn()
    {
        soundEffect.PlayOneShot(clickButton);
        Time.timeScale = 1.0F;
        SceneManager.LoadScene("menuScene");
        DataHandler.Instance.totalPoints = 0;
    }

    //Metoda umozliwiajaca zaladowanie nastepnego poziomu po ukonczeniu biezacego poziomu gry
    //Wywolywana po nacisnieciu przyciusku play menu finish
    public void NextMap()
    {
        soundEffect.PlayOneShot(clickButton);
        Time.timeScale = 1.0F;
        if (DataHandler.Instance.currentMap < DataHandler.Instance.arrayFileMap.Length - 1)
            DataHandler.Instance.currentMap++;
        else
            DataHandler.Instance.currentMap = 0;
        DataHandler.Instance.TextAssetToList(DataHandler.Instance.arrayFileMap[DataHandler.Instance.currentMap]);
        SceneManager.LoadScene("loadingScene");
    }
}
