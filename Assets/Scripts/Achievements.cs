﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Achievements : DataReg {

    private static Achievements instance;

    /// <summary>
    /// Lista przechowująca osiągnięcia
    /// </summary>
    [SerializeField]
    private List<string> listAchievements = new List<string>();

    /// <summary>
    /// Lista przechowująca stany osiągnięć. (True - odblokowane, False - zablokowane)
    /// </summary>
    [SerializeField]
    private List<bool> listAchievementState = new List<bool>();

    public static Achievements Instance
    {
        get
        {
            if (instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<Achievements>();
            }
            return instance;
        }
    }

    public List<bool> ListAchievementState
    {
        get
        {
            return listAchievementState;
        }
    }

    public List<string> ListAchievements
    {
        get
        {
            return listAchievements;
        }

        set
        {
            listAchievements = value;
        }
    }

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start () {
        DontDestroyOnLoad(this);

        if (FindObjectsOfType(GetType()).Length > 1)
            Destroy(gameObject);

        TakeBoolsToArray("AchievementsStatesArray", ListAchievementState, ListAchievements.Count);
    }

    /// <summary>
    /// Metoda pozwalająca odblokować osiągnięcie. Jest uruchamiana po spełneniu określonych warunków, gdy gracz ukończy poziom.
    /// </summary>
    /// <param name="achieve_id">Id osiągnięcia z tablicy tablicy osiągnięć</param>
    public void UnlockAchieve(int achieve_id)
    {
        ListAchievementState[achieve_id] = true;
    }

    /// <summary>
    /// Metoda pobierająca wartości typu bool z pliku rejestrowego gry.
    /// </summary>
    /// <param name="key">Nazwa klucza w pliku rejestrowym</param>
    /// <param name="listState">Nazwa listy, która będzie przechowywać pobrane wartości</param>
    void TakeStateOfAchievementsFromFile(string key, List<bool> listState)
    {
        //pobieram stany osiągnięć z pliku rejestrowego gry
        if (PlayerPrefs.HasKey(key))
        {
            for (int i = 0; i < ListAchievements.Count; i++)
                listState.Add(PlayerPrefsX.GetBoolArray(key)[i]);
        }
        else    //jesli nie ma pliku (gra zostala uruchomina 1 raz)
        {
            for (int i = 0; i < ListAchievements.Count; i++)
                listState.Add(false);

            bool[] tmpStateArray = listState.ToArray();
            PlayerPrefsX.SetBoolArray(key, tmpStateArray);
        }
    }


}
