﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTrigger : MonoBehaviour {

    /// <summary>
    /// Collider playera
    /// </summary>
    [SerializeField]
    private BoxCollider2D playerCollider;

    [SerializeField]
    private GameObject[] enemyObjects;

    /// <summary>
    /// Collider platformy (górny), który nie jest triggerem
    /// </summary>
    public BoxCollider2D platformCollider;

    /// <summary>
    /// Collider platformy (dolny), który jest triggerem
    /// </summary>
    public BoxCollider2D platformTrigger;

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start () {
        playerCollider = GameObject.FindGameObjectWithTag("Player").GetComponent<BoxCollider2D>();
        enemyObjects = GameObject.FindGameObjectsWithTag("Enemy");

        //ignorowanie kolizji między nałożonymi na siebie boxcolliderami
        Physics2D.IgnoreCollision(platformCollider, platformTrigger, true);
    }
    
    /// <summary>
    /// Metoda uruchamiająca się, gdy obiekt (other) wchodzi w obszar collidera
    /// </summary>
    /// <param name="other">Obiekt, który narusza collider</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Physics2D.IgnoreCollision(platformCollider, playerCollider, true);
        }

        if (other.gameObject.tag == "Enemy")
        {
            foreach(GameObject g in enemyObjects)
                Physics2D.IgnoreCollision(platformCollider, g.GetComponent<BoxCollider2D>(), true);

        }
    }
    /// <summary>
    /// Metoda uruchamiająca się, gdy obiekt (other) wychodzi z obszaru collidera
    /// </summary>
    /// <param name="other">Obiekt, który narusza collider</param>
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Physics2D.IgnoreCollision(platformCollider, playerCollider, false);
        }

        if (other.gameObject.tag == "Enemy")
        {
            foreach (GameObject g in enemyObjects)
                Physics2D.IgnoreCollision(platformCollider, g.GetComponent<BoxCollider2D>(), false);

        }
    }
    

}
