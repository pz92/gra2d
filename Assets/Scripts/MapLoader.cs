﻿using System.Collections.Generic;
using UnityEngine;

public class MapLoader : DataReg {

    /// <summary>
    /// Tablica zdjęć map
    /// </summary>
    public Sprite[] arrayPicturesMaps;      //tablica zdjęć do map
    
    /// <summary>
    /// Ilość map
    /// </summary>
    private int countMaps;

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>    
    void Start()
    {
        LoadMapToArray("Maps/files");
        LoadPhotosToArray("Maps/images");

        countMaps = DataHandler.Instance.arrayFileMap.Length;


        for (int i = 0; i < countMaps; i++)
            DataHandler.Instance.listStateMap.Add(false);
        DataHandler.Instance.listStateMap[0] = true;

        TakeIntsToArray("MapsRecordsArray", DataHandler.Instance.listBestScore, countMaps);
         
    }

    /// <summary>
    /// Metoda dodaje pliki map (.txt) do tablicy znajdujacej sie w klasie DataHandler
    /// </summary>
    /// <param name="path">Ścieżka do katalogu z mapami</param>
    void LoadMapToArray(string path)
    {
        DataHandler.Instance.arrayFileMap = Resources.LoadAll<TextAsset>(path);
    }

    /// <summary>
    /// Metoda dodaje zdjecia map do tablicy arrayPictureMaps
    /// </summary>
    /// <param name="path"></param>
    void LoadPhotosToArray(string path)
    {
        //pobieram zdjecia map do tablicy (uzyte w menu)
        arrayPicturesMaps = Resources.LoadAll<Sprite>(path);
    }
}
