﻿using System.Collections;
using UnityEngine;

public delegate void DeadEventHandler();


public class Player : Character {

    /// <summary>
    /// Obiekt audio
    /// </summary>
    public AudioSource soundBackground;

    /// <summary>
    /// Pozycja startowa gracza
    /// </summary>
    public Transform startPosition;
    public event DeadEventHandler Dead;
    /// <summary>
    /// Panel PowerUP
    /// </summary>
    public GameObject PanelPowerUP;

    private static Player instance;
    public static Player Instance
    {
        get
        {
            if(instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<Player>();
            }
            return instance;
        }
    }

    /// <summary>
    /// Tablica punktów ustawionych przy stopach playera
    /// </summary>
    [SerializeField]
    //punkty ustawione przy stopach playera które umożliwią sprawdzenie czy player jest na ziemi
    private Transform[] groundPoints;

    /// <summary>
    /// Promień colliderów punktów przy stopach playera
    /// </summary>
    [SerializeField]
    //promień colliderów ww punktów
    private float groundRadious;

    /// <summary>
    /// Przechowuje dane co jest traktowane jako ziemia
    /// </summary>
    [SerializeField]
    //przechowuje dane co jest traktowane jako ziemia
    private LayerMask whatIsGround;

    /// <summary>
    /// Sterowanie postacią podczas lotu
    /// </summary>
    [SerializeField]
    //sterowanie postasią podczas lotu
    private bool airControl;

    /// <summary>
    /// Siła skoku
    /// </summary>
    [SerializeField]
    //siła skoku
    private float jumpForce;

    /// <summary>
    /// Kierunek poruszania postacią
    /// </summary>
    public float direction;

    /// <summary>
    /// Postać się porusza lub nie
    /// </summary>
    public bool move;

    //skok postaci
    /// <summary>
    /// Postać skacze lub nie
    /// </summary>
    public bool Jump;

    //czy jest na ziemi
    /// <summary>
    /// Postać jest na ziemi lub nie
    /// </summary>
    public bool OnGround;

	/// <summary>
    /// Rigidbody postaci gracza
    /// </summary>
    public Rigidbody2D MyRigidbody;

    [SerializeField]
    private Stat healthStat;

    /// <summary>
    /// Czas w powietrzu
    /// </summary>
    private float timeInAir = 0;
    /// <summary>
    /// Czas podwójne szybkości
    /// </summary>
    private float powerTime = 0;

    /// <summary>
    /// Ilość pokonanych przeciwników
    /// </summary>
    public int killedEnemy = 0;
    /// <summary>
    /// Ilość zebranych przedmiotów
    /// </summary>
    public int collectedItem = 0;
    /// <summary>
    /// Czy zostało zebrane serduszko
    /// </summary>
    public bool healthItem = false;
    /// <summary>
    /// Czy została zebrana moneta
    /// </summary>
    public bool coinItem = false;
    /// <summary>
    /// Czy gracz odniósł obrażenia
    /// </summary>
    public bool damageState = false;
    /// <summary>
    /// Czy gracz atakował mieczem
    /// </summary>
    public bool swordAttack = false;
    /// <summary>
    /// Czy gracz atakował strzałami
    /// </summary>
    public bool arrowAttack = false;
    /// <summary>
    /// Czy gracz zebrał dopalacz
    /// </summary>
    public bool powerItem = false;

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>    
    public override void Start() 
	{
        Time.timeScale = 1.0F;
        base.Start ();
        //dostęp do komponentu rigidbody postaci
        MyRigidbody = GetComponent<Rigidbody2D>();
        healthStat.Initialize();
        ScorllBG.Instance.Rig = MyRigidbody;
    }

    /// <summary>
    /// Metoda uruchamiająca się w każdej klatce
    /// </summary>    
    void Update()
    {
        if (!TakingDamage && !IsDead)
        {
            if (transform.position.y <= -20f && gameObject.tag == "Player")
            {
                gameObject.tag = "Untagged";
                soundBackground.volume = 0;
                soundEffect.PlayOneShot(listSounds[3]);
                Death();
            }
            HandleInput();
        }
    }

    /// <summary>
    /// Metoda uruchamiająca się w odstępach określonego czasu
    /// </summary>  
    void FixedUpdate() {
        if (startPosition == null)
        {
            startPosition = GameObject.FindGameObjectWithTag("StartPosition").GetComponent<Transform>();
            gameObject.transform.position = new Vector2(startPosition.position.x, startPosition.position.y);
        }

        if (!TakingDamage && !IsDead)
        {
            //pobranie wartości przesunięcia postaci w osi X... -1..0..1
            //direction = Input.GetAxis("Horizontal");

            OnGround = IsGrounded();
            HandleMovement(direction);
            Flip(direction);
            HandleLayers();
        }
        if(!OnGround)
            InAir();

        if(powerItem)
        {
            soundBackground.pitch = 1.5f;
            powerUp();
        }
    }

    /// <summary>
    /// Metoda umożliwiająca poruszanie postacią
    /// </summary>
    /// <param name="horizontal">Kierunek poruszania postacią <-1, 1></param>
    void HandleMovement(float horizontal) {
        //jeśli postać spada, wartość velocity.y jest ujemna gdy postać porusza się w doł osi Y
        if(MyRigidbody.velocity.y < 0)
        {
            //aktywuję animację lądowania
            MyAnimator.SetBool("land", true);
        }
      
        //jeśli nie atakuję,  jestem na ziemi lub mam włączoną możliwość sterowania postacią w locie
        if(!Attack && (OnGround || airControl))
        {
            //poruszam postacią po osi X, nie zmieniająć wartości osi Y, nie jest możliwy bieg i machanie mieczem
            MyRigidbody.velocity = new Vector2(horizontal * movementSpeed, MyRigidbody.velocity.y);
        }
        //jeśli wywołam skok i postać stoi (w osi Y) 
        if (Jump && (Mathf.Round(MyRigidbody.velocity.y * 100f) / 100f) == 0 )
        {
            //nadaję siłę postaci w osi Y
            MyRigidbody.AddForce(new Vector2(0, jumpForce));
            Jump = false;
        }

        //animator przechowuje wartość bezwzględną porusznia się postaci w osi X w zmiennej speed
        //dzięki temu aktywuje się animacja biegu gdy prędkość poruszania postaci jest większa niż 0
        MyAnimator.SetFloat("speed", Mathf.Abs(direction));
    }

    /// <summary>
    /// Metoda służąca do odczytywania klawiszy z klawiatury
    /// </summary>
    private void HandleInput()
    {
        //lewy shift to walka mieczem
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            swordAttack = true;
            MyAnimator.SetTrigger("attack");
        }
        //spacja to skok
        if (Input.GetKeyDown(KeyCode.Space))
        {
            MyAnimator.SetTrigger("jump");
        }

        //klawisz Z to rzucienie noża
        if(Input.GetKeyDown(KeyCode.Z))
        {
            arrowAttack = true;
            MyAnimator.SetTrigger("throw");
        }
    }

    /// <summary>
    /// Metoda służąca do zmiany kierunku patrzenia postaci
    /// </summary>
    /// <param name="horizontal">Kierunek poruszania postacią <-1, 1></param>
    void Flip(float horizontal)
    {
        //jeśli postać biegnie w prawo i sprite jest lewy lub postać biegnie w lewo i sprite jest prawy to następuje zamiana
        if (horizontal > 0 && !facingRight || horizontal < 0 && facingRight)
        {
			ChangeDirection ();
        } 
    }

    /// <summary>
    /// Metoda sprawdzająca czy postać jest na ziemi
    /// </summary>
    /// <returns></returns>
    bool IsGrounded()
    {
        //jeśli postać spada lub stoi (velocity.y <= 0)
        if (MyRigidbody.velocity.y <= 0.05)
        {
            //do punktów sprawdzających czy postać stoi
            foreach (Transform point in groundPoints)
            {
                //dodaję circleCollidery
                Collider2D[] colliders = Physics2D.OverlapCircleAll(point.position, groundRadious, whatIsGround);

                //następnie sprawdzam czy któryś circleCollider wykrył jakiś obiekt
                for (int i = 0; i < colliders.Length; i++)
                {
                    //obiekt ten nie może być playerem :)
                    if (colliders[i].gameObject != gameObject)  //!=gameobject <- to obiekt playera więc jest ignorowany
                    {
                        return true;
                    }
                }
            }
        }
        return false;         
    }

    //Metoda służąca do przełączania między layearmi animatora
    //Jeśli player jest w powietrzu: aktywowane są animacje z layera 1 (airLayer), gdzie są animacje skoku, lądowania, ataku w powietrzu
    //Jeśli player jest na ziemi: aktywowane są animacje z layera 0 (groundLayer), gdzie są animacje m.in. biegu
    
        /// <summary>
    /// Metoda służąca do przełączania między layerami animatora
    /// </summary>
    void HandleLayers()
    {
        //jeśli postać jest w locie to aktywuję layer AirControl
        if (!OnGround)
            MyAnimator.SetLayerWeight(1, 1);    //(numer layera, jego waga)
        else
            MyAnimator.SetLayerWeight(1, 0);
    }

    /// <summary>
    /// Metoda wywolywana, gdy player odniesie obrazania dokonane przez przeciwnika
    /// </summary>
    public override void TakeDamage()
    {
        healthStat.CurrentVal -= 10;
        damageState = true;
        if(!IsDead)
        {
            MyAnimator.SetTrigger("damage");
        }
        else
        {
            soundBackground.volume = 0;
            soundEffect.PlayOneShot(listSounds[3]);
            MyAnimator.SetLayerWeight(1, 0);
            MyAnimator.SetTrigger("die");
        }
    }

    public override bool IsDead
    {
        get
        {
            if(healthStat.CurrentVal <= 0)
                OnDead();

            return healthStat.CurrentVal <= 0;
        }
    }

    public void OnDead()
    {
        if(Dead != null)
        {
            Dead();
        }
    }

    /// <summary>
    /// Metoda wywołana, gdy postac gracza stracila cale zycie
    /// </summary>
    public override void Death()
    {
        gameObject.tag = "Untagged";
        PanelPowerUP.SetActive(false);
        Finish.Instance.GameOver();
    }


    /// <summary>
    /// Metoda liczaca czas gracza spedzony w powietrzu
    /// </summary>
    void InAir()
    {
        if(timeInAir <= 10)
            timeInAir += Time.deltaTime;

        if (timeInAir >= 10)
            Achievements.Instance.UnlockAchieve(8);
    }

    /// <summary>
    /// Metoda zwiekszajaca prędkosc poruszania postacia
    /// </summary>
    void powerUp()
    {
        if (powerTime <= 5)
            powerTime += Time.deltaTime;
        else
        {
            soundBackground.pitch = 1;
            PanelPowerUP.SetActive(false);
            movementSpeed = movementSpeed / 2;
            powerTime = 0;
            powerItem = false;
        }

    }

    //Metoda sprawdzajaca czy collider postaci zostal naruszony przez inny collider (other)
    //Uzupelnienie metody z klasy nadrzednej
    //Sprawdzanie czy player zebral monete, apteczke lub inny przedmiot
    /// <summary>
    /// Metoda sprawdzajaca czy collider postaci zostal naruszony przez inny collider (other).
    /// </summary>
    /// <param name="other"></param>
    public override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);

        if(!IsDead)
        {
            switch (other.tag)
            {
                case "Coin":
                    soundEffect.pitch = 1;
                    soundEffect.PlayOneShot(listSounds[0]);
                    GameManager.Instance.Points += 100;
                    GameManager.Instance.TotalPoints += 100;
                    Destroy(other.gameObject);
                    collectedItem++;
                    coinItem = true;
                    if (collectedItem > 0 && Achievements.Instance.ListAchievementState[1] == false)
                        Achievements.Instance.UnlockAchieve(1);
                    break;

                case "Health":
                    soundEffect.pitch = 2;
                    soundEffect.PlayOneShot(listSounds[0]);
                    GameManager.Instance.TotalPoints += 1000;
                    GameManager.Instance.Points += 1000;
                    healthStat.CurrentVal = healthStat.MaxVal;
                    Destroy(other.gameObject);
                    healthItem = true;
                    break;

                case "Power":
                    soundEffect.pitch = 7f;
                    soundEffect.PlayOneShot(listSounds[0]);
                    GameManager.Instance.TotalPoints += 1500;
                    GameManager.Instance.Points += 1500;
                    if (!powerItem)
                        movementSpeed *= 2;
                    Destroy(other.gameObject);
                    powerItem = true;
                    PanelPowerUP.SetActive(true);
                    break;

                case "FakeHealth":
                    soundEffect.pitch = 3f;
                    soundEffect.PlayOneShot(listSounds[4]);
                    if (healthStat.CurrentVal >= healthStat.MaxVal / 2)
                        healthStat.CurrentVal = healthStat.CurrentVal / 2;

                    Destroy(other.gameObject);
                    break;
            }
        }
         
    }
}
