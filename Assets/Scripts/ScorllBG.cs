﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScorllBG : MonoBehaviour {

    private static ScorllBG instance;

    /// <summary>
    /// Prędkość przewijania tła
    /// </summary>
    [SerializeField]
    private float velocity = 0.0008f;

    /// <summary>
    /// Obiekt kamery
    /// </summary>
    [SerializeField]
    private Transform cam;
    /// <summary>
    /// Rigidbody śledonej postaci
    /// </summary>
    [SerializeField]
    private Rigidbody2D rig;
    /// <summary>
    /// Przesunięcie tekstury
    /// </summary>
    private Vector2 currentOffset;


    public static ScorllBG Instance
    {
        get
        {
            if (instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<ScorllBG>();
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }

    public Rigidbody2D Rig
    {
        set
        {
            rig = value;
        }
    }

    // Update is called once per frame
    /// <summary>
    /// Metoda wywoływana w określonych odstępach czasu
    /// </summary>
    void FixedUpdate () {
        //scroll tła 
        /*currentOffset = GetComponent<Renderer>().material.mainTextureOffset;
        if (rig.velocity.x != 0 && !Player.Instance.IsDead && Camera.Instance.cameraMoving)
        {
            currentOffset.x += rig.velocity.x * velocity;
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(currentOffset.x, 0);
        }
        */
        GetComponent<Transform>().position = new Vector2(cam.position.x, -8.09f);
	}
}
