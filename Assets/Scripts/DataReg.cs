﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReg : MonoBehaviour {

    /// <summary>
    /// Metoda umożliwia pobranie wartości listy typu bool z pliku rejestrowego gry, gdy nie ma takiej tablicy tworzy ją i wypełnia wartościami false
    /// </summary>
    /// <param name="key">Nazwa klucza w pliku rejestrowym</param>
    /// <param name="listName">Nazwa listy, która będzie przechowywać pobrane wartości</param>
    /// <param name="lengthList">Długość listy</param>
    protected void TakeBoolsToArray(string key, List<bool> listName, int lengthList, int trueIndex = -1)
    {
        //pobieram stany map z pliku rejestrowego gry
        if (PlayerPrefs.HasKey(key))
        {
            for (int i = 0; i < lengthList; i++)
                listName.Add(PlayerPrefsX.GetBoolArray(key)[i]);
        }
        else    //jesli nie ma pliku (gra zostala uruchomina 1 raz)
        {
            for (int i = 0; i < lengthList; i++)
                listName.Add(false);

            if(trueIndex != -1)
                listName[trueIndex] = true;

            bool[] tmpStateArray = listName.ToArray();
            PlayerPrefsX.SetBoolArray("MapsStatesArray", tmpStateArray);
        }
    }

    /// <summary>
    /// Metoda umożliwia pobranie wartości listy typu int z pliku rejestrowego gry, gdy nie ma takiej tablicy tworzy ją i wypełnia wartościami 0
    /// </summary>
    /// <param name="key">Nazwa klucza w pliku rejestrowym</param>
    /// <param name="listName">Nazwa listy, która będzie przechowywać pobrane wartości</param>
    /// <param name="lengthList">Długość listy</param>
    protected void TakeIntsToArray(string key, List<int> listName, int lengthList)
    {
        //pobieram wyniki map z pliku rejestrowego gry
        if (PlayerPrefs.HasKey(key))
        {
            for (int i = 0; i < lengthList; i++)
                listName.Add(PlayerPrefsX.GetIntArray(key)[i]);
        }
        else    //jesli nie ma pliku (gra zostala uruchomina 1 raz)
        {
            for (int i = 0; i < lengthList; i++)
                listName.Add(0);

            int[] tmpBestScoreArray = DataHandler.Instance.listBestScore.ToArray();
            PlayerPrefsX.SetIntArray(key, tmpBestScoreArray);
        }
    }
}
