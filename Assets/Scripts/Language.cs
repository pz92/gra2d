﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using UnityEngine.SceneManagement;

public class Language : MonoBehaviour {

    private static Language instance;
    
    /// <summary>
    /// Plik XML
    /// </summary>
    private XmlDocument xmlFile;

    /// <summary>
    /// Lista polskich wyrazów
    /// </summary>
    [SerializeField]
    private List<string> listLangPL = new List<string>();
    
    /// <summary>
    /// Lista angielskich wyrazów
    /// </summary>
    [SerializeField]
    private List<string> listLangEN = new List<string>();
    
    /// <summary>
    /// Lista osiągnięć w języku polski
    /// </summary>
    [SerializeField]
    private List<string> listAchievementsPL = new List<string>();

    /// <summary>
    /// Lista osiągnięć w języku angielskim
    /// </summary>
    [SerializeField]
    private List<string> listAchievementsEN = new List<string>();


    /// <summary>
    /// Lista obiektów, które mają zostać przetłumaczone
    /// </summary>
    [SerializeField]
    private List<GameObject> listObjects = new List<GameObject>();

    public static Language Instance
    {
        get
        {
            if (instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<Language>();
            }
            return instance;
        }

        set
        {
            instance = value;
        }
    }
    
    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start() {
        xmlFile = OpenXMLFile("Language/Lang");
        ReadFromXML(xmlFile, listLangPL, listLangEN, SceneManager.GetActiveScene().name);
        ReadFromXML(xmlFile, listAchievementsPL, listAchievementsEN, "AllAchievements");

        //ReadFromXML(xmlFile, listLangEN);

        Resources.LoadAll<Sprite>("Maps/images");

        if (SceneManager.GetActiveScene().name != "gameScene")
            SetLanguage(DataHandler.Instance.currentLanguage);
        else
            SetLanguageGameScene(0, 0);
    }

    /// <summary>
    /// Metoda ustawiająca wybrany język
    /// </summary>
    /// <param name="lang">Skrót język, który chcemy aktywować (PL, EN)</param>
    public void SetLanguage(string lang)
    {
        if (lang == "PL")
        {
            for (int i = 0; i < listLangPL.Count; i++)
                listObjects[i].GetComponentInChildren<Text>().text = listLangPL[i];

            AddAchievements("PL", Achievements.Instance.ListAchievements);
        }
        else
        {
            for (int i = 0; i < listLangPL.Count; i++)
                listObjects[i].GetComponentInChildren<Text>().text = listLangEN[i];

            AddAchievements("EN", Achievements.Instance.ListAchievements);
        }
    }

    /// <summary>
    /// Specjalna metoda ustawienia języka, która umożliwia ustawienie odpowiedniego słowa w wybranym labelu
    /// </summary>
    /// <param name="gameObjIndex">Id obiektu z listy obiektów, do którego chcemy przypisać słowo</param>
    /// <param name="wordIndex">Id słowa z listy słów, które chcemy przypisać do obiektu</param>
    /// <param name="optText">Tekst jaki checemy dokleić do pobranego słowa (parametr opcjonalny)</param>
    public void SetLanguageGameScene(int gameObjIndex, int wordIndex, string optText = "")
    {
        if (DataHandler.Instance.currentLanguage == "PL")
            listObjects[gameObjIndex].GetComponentInChildren<Text>().text = listLangPL[wordIndex] + optText;

        else
            listObjects[gameObjIndex].GetComponentInChildren<Text>().text = listLangEN[wordIndex] + optText;
    }





    /// <summary>
    /// Metoda dodająca opisy osiągnięć do listy osiągnięć
    /// </summary>
    /// <param name="lang">Skrót języka (PL, EN) w jakim mają zostać pobrane opisy</param>
    /// <param name="ListAchievements">Lista do której mają zostać wczytane osiągnięcia</param>
    void AddAchievements(string lang, List<string> ListAchievements)
    {
        int countAchievements = listAchievementsPL.Count;

        if (lang == "PL")
        {
            //jesli list jest juz wypełniona to podmieniam stringi
            if(ListAchievements.Count > 0)
            {
                for (int i = 0; i < countAchievements; i++)
                    ListAchievements[i] = listAchievementsPL[i];
            }
            //jesli nie to dodaje stringi do listy
            else
            {
                for (int i = 0; i < countAchievements; i++)
                    ListAchievements.Add(listAchievementsPL[i]);
            }       
        } 
        else
        {
            //jesli list jest juz wypełniona to podmieniam stringi
            if (ListAchievements.Count > 0)
            {
                for (int i = 0; i < countAchievements; i++)
                    ListAchievements[i] = listAchievementsEN[i];
            }
            //jesli nie to dodaje stringi do listy
            else
            {
                for (int i = 0; i < countAchievements; i++)
                    ListAchievements.Add(listAchievementsEN[i]);
            }
        }
    }


    /// <summary>
    /// Metoda otwierająca plik XML
    /// </summary>
    /// <param name="path">Ścieżka do pliku XML</param>
    /// <returns>Zwraca otwarty plik XML</returns>
    XmlDocument OpenXMLFile(string path)
    {
        TextAsset textAsset = (TextAsset)Resources.Load(path);
        XmlDocument xml = new XmlDocument();
        xml.LoadXml(textAsset.text);

        return xml;
    }


    /// <summary>
    /// Metoda wczytująca słowa z pliku XML do wybranej listy
    /// </summary>
    /// <param name="xmlFile">Plik xml z którego chcemy wczytać dane</param>
    /// <param name="listLangPL">Lista do której chcemy wczytać polskie słowa</param>
    /// <param name="listLangEN">Lista do której chcemy wczytać angielskie słowa</param>
    /// <param name="tagXML">Tag w pliku xml, który chemy wczytać</param>
    void ReadFromXML(XmlDocument xmlFile, List<string> listLangPL, List<string> listLangEN, string tagXML)
    {
            for (int i = 0; i < xmlFile.GetElementsByTagName(tagXML).Item(0).ChildNodes.Count; i++)
            {
                listLangPL.Add(xmlFile.GetElementsByTagName(tagXML).Item(0).ChildNodes[i].InnerText);
                listLangEN.Add(xmlFile.GetElementsByTagName(tagXML).Item(1).ChildNodes[i].InnerText);
            }

        //odwołanie do pierszego elementu w menu
        //Debug.Log(xml.GetElementsByTagName("Menu").Item(0).ChildNodes[0].InnerText);

        //odwołanie do pierszego elementu w menu
        //Debug.Log(xml.GetElementsByTagName("Menu").Item(0).ChildNodes.Item(0).InnerText);

        //liczenie elementów menu
        //Debug.Log(xml.GetElementsByTagName("Menu").Item(0).ChildNodes.Count);
    }
}
