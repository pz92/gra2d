﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class LoadingGame : MonoBehaviour {

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start () {
    SceneManager.LoadScene("gameScene");
    }
}
