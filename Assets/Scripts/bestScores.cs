﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class bestScores : MonoBehaviour
{
    private static bestScores instance;

    /// <summary>
    /// Lista najelpszych wyników
    /// </summary>
    [SerializeField]
    private List<Text> listTextScores;

    public static bestScores Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<bestScores>();
            }
            return instance;
        }

    }

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start()
    {
        TakeBestScoresFromFile("HighestScores", DataHandler.Instance.listHighestScores);
    }

    /// <summary>
    /// Metoda akutalizująca listę najlepszych wyników
    /// </summary>
    public void UpdateListHighScores()
    {
        DataHandler.Instance.listHighestScores.Sort();
        DataHandler.Instance.listHighestScores.Reverse();
        for (int i = 0; i < 7; i++)
        {
            if(DataHandler.Instance.listHighestScores[i] != 0)
                listTextScores[i].text = DataHandler.Instance.listHighestScores[i].ToString();
            else
                listTextScores[i].text = "-";
        }
    }

    /// <summary>
    /// Metoda pobierająca wartości typu int z pliku rejestrowego gry
    /// </summary>
    /// <param name="key">Nazwa klucza w pliku rejestrowym</param>
    /// <param name="listState">Nazwa listy, która będzie przechowywać pobrane wartości</param>
    void TakeBestScoresFromFile(string key, List<int> listScore)
    {
        //pobieram z pliku rejestrowego gry
        if (PlayerPrefs.HasKey(key))
        {
            for (int i = 0; i < 7; i++)
                listScore.Add(PlayerPrefsX.GetIntArray(key)[i]);
        }
        else    //jesli nie ma pliku (gra zostala uruchomina 1 raz)
        {
            for (int i = 0; i < 7; i++)
                listScore.Add(0);

            int[] tmpScoresArray = listScore.ToArray();
            PlayerPrefsX.SetIntArray(key, tmpScoresArray);
        }
    }

}