﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Character : MonoBehaviour {


    /// <summary>
    /// Lista efektów dźwiękowych
    /// </summary>
    public List<AudioClip> listSounds;

    /// <summary>
    /// Obiekt audio
    /// </summary>
    public AudioSource soundEffect;

    /// <summary>
    /// Animator postaci
    /// </summary>
    protected Animator myAnimator;

    public Animator MyAnimator { get; set; }

    /// <summary>
    /// Pozycja, z której będzie wyrzucany nóż
    /// </summary>
    [SerializeField]
	//pozycja z której będzie wystrzeliwany nóż
	protected Transform knifePos;

    /// <summary>
    /// Prędkosc poruszania postaci
    /// </summary>
    [SerializeField]
    protected float movementSpeed;

	/// <summary>
    /// Kierunek, w którym "patrzy" postać. (True - prawo, False - lewo)
    /// </summary>
    protected bool facingRight;

	/// <summary>
    /// Prefab noża
    /// </summary>
    [SerializeField]
	protected GameObject knifePrefab;

    /// <summary>
    /// Wartość życia
    /// </summary>
    [SerializeField]
    protected int health;

    /// <summary>
    /// Collider miecza
    /// </summary>
    [SerializeField]
    private EdgeCollider2D swordCollider;

    /// <summary>
    /// Lista tagów obiektów, które mogą zranić postać
    /// </summary>
    [SerializeField]
    private List<string> damageSources;

    /// <summary>
    /// Postać żyje/nieżyje
    /// </summary>
    public abstract bool IsDead { get; }

	/// <summary>
    /// Atak mieczem lub nożem
    /// </summary>
    public bool Attack;


    /// <summary>
    /// Postać jest raniona
    /// </summary>
    public bool TakingDamage { get; set; }

    public EdgeCollider2D SwordCollider
    {
        get
        {
            return swordCollider;
        }
    }


    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>    
    public virtual void Start () {
        //kierunek spirta, domyśle postać zwrócona w prawo
        facingRight = true;

		//dostęp do komponentu Animator przypisanego do postaci
		MyAnimator = GetComponent<Animator>();

        soundEffect = GameObject.Find("EffectSound").GetComponent<AudioSource>();

    }

    /// <summary>
    /// Metoda określająca zachowanie postaci, gdy jest atakowana
    /// </summary>
    public abstract void TakeDamage();

    /// <summary>
    /// Metoda określająca zachowanie postaci, gdy umiera
    /// </summary>
    public abstract void Death();

    /// <summary>
    /// Metoda pozwalajaca zmienic kierunek sprite'a postaci
    /// </summary>
    public void ChangeDirection()
	{
		facingRight = !facingRight;
		transform.localScale = new Vector3 (transform.localScale.x * -1, 0.4f, 1);
	}

    /// <summary>
    /// Metoda służąca do stworzenia obiektu lecącego noża
    /// </summary>
    public virtual void ThrowKnife()
	{
        soundEffect.PlayOneShot(listSounds[2]);
        //jesli postac jest zwrocona w prawo
        if (facingRight)
		{
			//tworze obiekt, wrzucam go na scenę z prefabm noża i odpowiednią pozycją i rotacją (Quaternion.Euler zwraca mi obiekt obrócony w wybranej osi)
			GameObject tmp = (GameObject)Instantiate(knifePrefab, knifePos.position , Quaternion.Euler(new Vector3(0, 0, -90)));
			//ustawiam wektor poruszania noża (1,0), bo bez tego obiekt stałby w miejscu i się nie poruszał :)
			tmp.GetComponent<Knife>().Initialize(Vector2.right);
		}
		//jeśli postać jest na ziemi i wartość jest 0, to w 0.2 sek animacji rzutu, noż się pojawi 
		else
		{
			//tworze obiekt, wrzucam go na scenę z prefabm noża i odpowiednią pozycją i rotacją (Quaternion.Euler zwraca mi obiekt obrócony w wybranej osi)
			GameObject tmp = (GameObject)Instantiate(knifePrefab, knifePos.position , Quaternion.Euler(new Vector3(0, 0, 90)));
			//ustawiam wektor poruszania noża (-1,0)
			tmp.GetComponent<Knife>().Initialize(Vector2.left);
		}
	}

    /// <summary>
    /// Metoda wlaczajaca collider miecza
    /// </summary>
    public void MeleeAttack()
    {
        soundEffect.PlayOneShot(listSounds[1]);
        SwordCollider.enabled = true;
    }

    /// <summary>
    /// Metoda sprawdzajaca czy collider postaci zostal naruszony przez inny collider (other)
    /// </summary>
    /// <param name="other"></param>
    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if(damageSources.Contains(other.tag))
        {
            TakeDamage();
        }
    }
}
