﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    private static GameManager instance;

    /// <summary>
    /// Obiekt wyświetlający ilość zdobytych punktów 
    /// </summary>
    [SerializeField]
    private Text scoreText;
    /// <summary>
    /// Lista postaci gracza
    /// </summary>
    [SerializeField]
    private List<GameObject> listPlayers;

    /// <summary>
    /// Ilość punktów zdobyta na danej mapie
    /// </summary>
    [SerializeField]
    private int points;
    /// <summary>
    /// Całkowita ilość punktów zdobyta w ciągu gry
    /// </summary>
    [SerializeField]
    private int totalPoints;

    /// <summary>
    /// Obiekt kamery
    /// </summary>
    [SerializeField]
    private GameObject cam;

    public static GameManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<GameManager>();
            }
            return instance;
        }
    }

    /// <summary>
    /// Lista prefabów przedmiotów
    /// </summary>
    public List<GameObject> ItemsPrefab;

    public int Points
    {
        get
        {
            return points;
        }

        set
        {
            points = value;
        }
    }

    public int TotalPoints
    {
        get
        {
            return totalPoints;
        }

        set
        {
            totalPoints = value;
            scoreText.text = totalPoints.ToString();
        }
    }

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start()
    {
        TotalPoints = DataHandler.Instance.totalPoints;
        foreach (GameObject player in listPlayers)
            if (player.name == DataHandler.Instance.selectedCharacter)
                player.SetActive(true);

        cam.SetActive(true);     
    }


}
