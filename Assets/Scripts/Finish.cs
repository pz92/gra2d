﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour {

    private static Finish instance;
    public static Finish Instance
    {
        get
        {
            if (instance == null)
            {
                //FindObjectOfType zwraca pierwszy znaleziony objekt danego typu
                instance = GameObject.FindObjectOfType<Finish>();
            }
            return instance;
        }

    }
    /// <summary>
    /// Zmienna przechowująca informację, czy gracz ukończył mapę
    /// </summary>
    public bool isFinish;
    /// <summary>
    /// Zmienna przechowująca informację, czy gracz został pokonany
    /// </summary>
    public bool isGameOver;

    /// <summary>
    /// Metoda uruchamiająca się przy aktywowaniu obiektu
    /// </summary>
    void Start()
    {
        isFinish = false;
        isGameOver = false;
    }

    /// <summary>
    /// Metoda uruchamiająca się, gdy obiekt (other) wchodzi w obszar collidera
    /// </summary>
    /// <param name="other">Obiekt, który narusza collider</param>
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Player.Instance.soundBackground.pitch = 1;
            FinishMap();
        }
    }

    /// <summary>
    /// Metoda uzupelniajaca pola tekstowe w Panelu konca gry (gdy jest game over lub finish)
    /// </summary>
    void FillScore()
    {
        DataHandler.Instance.totalPoints = GameManager.Instance.TotalPoints;

        if(isFinish)
        {
            //LEVEL CLEARED!
            Language.Instance.SetLanguageGameScene(1, 1);
            if (DataHandler.Instance.listBestScore[DataHandler.Instance.currentMap] < GameManager.Instance.Points)
            {
                DataHandler.Instance.listBestScore[DataHandler.Instance.currentMap] = GameManager.Instance.Points;
                //NEW HIGHSCORE!!!
                Language.Instance.SetLanguageGameScene(2, 4, "\n" + GameManager.Instance.Points.ToString());
            }
            else
            {
                //YOUR SCORE
                Language.Instance.SetLanguageGameScene(2, 3, "\n" + GameManager.Instance.Points.ToString());
            }
            PauseManager.Instance.ButtonPlay.SetActive(true);
        }

        if (isGameOver)
        {
            //wyświetlenie game over w labeleu TextCompleteLevel
            //game over
            Language.Instance.SetLanguageGameScene(1, 2);

            if (DataHandler.Instance.listHighestScores[DataHandler.Instance.listHighestScores.Count - 1] < GameManager.Instance.TotalPoints)
            {
                //NEW TOTAL HIGHSCORE!!!
                Language.Instance.SetLanguageGameScene(2, 6, "\n" + DataHandler.Instance.totalPoints.ToString());
                DataHandler.Instance.listHighestScores[DataHandler.Instance.listHighestScores.Count - 1] = GameManager.Instance.TotalPoints;
            }
            else
            {
                //YOUR TOTAL SCORE
                Language.Instance.SetLanguageGameScene(2, 5, "\n" + DataHandler.Instance.totalPoints.ToString());
            }
        }
    }

    //Metoda wywolywana jest gdy gracz straci cale zycie
    //wylacza i wlacza odpowiednie panele prezentujace menu
    //zatrzymuje czas gry 
    /// <summary>
    /// Metoda wywolywana jest gdy gracz straci cale życie
    /// </summary>
    public void GameOver()
    {
        isGameOver = true;
        FillScore();

        Player.Instance.PanelPowerUP.SetActive(false);
        PauseManager.Instance.ControlButtonsPanel.SetActive(false);
        PauseManager.Instance.PanelFinish.SetActive(true);
        PauseManager.Instance.ButtonPlay.SetActive(false);

        Player.Instance.tag = "Untagged";
    }

    //Metoda wykonujaca sie gdy gracz wejdzie w odpowiednie pole na mapie oznaczajace koniec mapy
    /// <summary>
    /// Metoda wykonujaca sie, gdy gracz wejdzie w odpowiednie pole na mapie oznaczajace koniec mapy
    /// </summary>
    void FinishMap()
    {
        GameObject.FindObjectOfType<ControllCharacter>().StopMove();
        isFinish = true;
        FillScore();

        Player.Instance.PanelPowerUP.SetActive(false);
        PauseManager.Instance.ControlButtonsPanel.SetActive(false);
        PauseManager.Instance.PanelFinish.SetActive(true);

        Player.Instance.tag = "Untagged";

        //odblokowuje nastepna mape
        if (DataHandler.Instance.currentMap < DataHandler.Instance.arrayFileMap.Length - 1)
            DataHandler.Instance.listStateMap[DataHandler.Instance.currentMap + 1] = true;

        CheckAchievements();
    }

    /// <summary>
    /// Metoda sprawdzająca czy nalezy odblokowac osiagniecie
    /// </summary>
    void CheckAchievements()
    {
        //jesli ukonczymy 1 poziom
        if (DataHandler.Instance.currentMap == 0)
        {
            Achievements.Instance.UnlockAchieve(0);
        }

        //jesli zabijemy 1 przeciwnika
        if (Player.Instance.killedEnemy >= DataHandler.Instance.countEnemy && DataHandler.Instance.countEnemy != 0)
        {
            Achievements.Instance.UnlockAchieve(4);
        }

        //jesli zbierzemy wszstkie przedmioty wraz z tymi ktore wypadaja z przeciwnika
        if (Player.Instance.collectedItem == DataHandler.Instance.countItems + DataHandler.Instance.countEnemy)
        {
            Achievements.Instance.UnlockAchieve(3);
        }

        //jesli nie zbierzemy apteczki
        if (!Player.Instance.healthItem)
        {
            Achievements.Instance.UnlockAchieve(6);
        }

        //jesli ukonczymy wszystkie poziomy
        if (DataHandler.Instance.arrayFileMap.Length - 1 == DataHandler.Instance.currentMap)
        {
            Achievements.Instance.UnlockAchieve(7);
        }

        //jesli nie zbierzemy zadnej monety
        if (!Player.Instance.coinItem)
        {
            Achievements.Instance.UnlockAchieve(9);
        }

        //jesli nie zrani nas przeciwnik
        if (!Player.Instance.damageState)
        {
            Achievements.Instance.UnlockAchieve(10);
        }

        //jesli przejdziemy plansze przez uzycia miecza
        if (!Player.Instance.swordAttack)
        {
            Achievements.Instance.UnlockAchieve(11);
        }

        //jesli przejdziemy plansze bez uzycia strzal
        if (!Player.Instance.arrowAttack)
        {
            Achievements.Instance.UnlockAchieve(12);
        }

        //sprawdzenie czy wszystkie osiagniecia sa odblokowane
        int listCount = Achievements.Instance.ListAchievementState.Count;
        int tmpCounter = 0;
        foreach (bool state in Achievements.Instance.ListAchievementState)
            if (state)
                tmpCounter++;

        if (listCount - 1 == tmpCounter)
            Achievements.Instance.UnlockAchieve(13);
    }

}
